//
//  main.cpp
//  DynamicMemory
//
//  Created by CavanSu on 2019/3/15.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>
#include "Box.hpp"

using namespace std;

// malloc() 函数在 C 语言中就出现了，在 C++ 中仍然存在，但建议尽量不要使用 malloc() 函数。new 与 malloc() 函数相比，其主要的优点是，new 不只是分配了内存，它还创建了对象。

void test1()
{
    double* pvalue  = NULL; // 初始化为 null 的指针
    
    if( !(pvalue  = new double ))
    {
        cout << "Error: out of memory." << endl;
        exit(1);
    }

    delete pvalue; // 释放内存
}

void test2()
{
    char* pvalue  = NULL;   // 初始化为 null 的指针
    pvalue  = new char[20]; // 为变量请求内存
    delete [] pvalue;        // 删除 pvalue 所指向的数组
}

void test3()
{
    int **p;
    int i,j;   //p[4][8]
    //开始分配4行8列的二维数据
    p = new int *[4];
    for(i = 0; i < 4; i++) {
        p[i] = new int[8];
    }
    
    for(i = 0; i < 4; i++) {
        for(j = 0; j < 8; j ++){
            p[i][j] = j * i;
        }
    }
    
    //打印数据
    for(i = 0; i < 4; i ++) {
        for(j = 0; j < 8; j++) {
            if(j == 0) {
                cout << endl;
            }
            cout << p[i][j] << "\t";
        }
    }
    //开始释放申请的堆
    for(i = 0; i < 4; i++){
        delete [] p[i];
    }
    delete [] p;
}

void test4()
{
    Box* myBoxArray = new Box[4];
    delete [] myBoxArray; // 删除数组
}

int main(int argc, const char * argv[]) {
    test1();
    test2();
    test3();
    test4();
    return 0;
}
