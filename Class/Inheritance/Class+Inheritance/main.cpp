//
//  main.cpp
//  Class+Inheritance
//
//  Created by CavanSu on 2019/1/10.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

using namespace std;

#pragma mark - 基类
class Shape
{
public:
    void setWidth(int w)
    {
        width = w;
    }
    void setHeight(int h)
    {
        height = h;
    }
protected:
    int width;
    int height;
};

#pragma mark - 派生类
// 几乎不使用 protected 或 private 继承，通常使用 public 继承

// 一个派生类继承了所有的基类方法，但下列情况除外：
// 基类的构造函数、析构函数和拷贝构造函数。
// 基类的重载运算符。
// 基类的友元函数。

class Rectangle: public Shape
{
public:
    int getArea()
    {
        return (width * height);
    }
};

#pragma mark - 多继承
// 基类 PaintCost
class PaintCost
{
public:
    int getCost(int area)
    {
        return area * 70;
    }
};

class Rectangle2: public Shape, public PaintCost
{
public:
    int getArea()
    {
        return (width * height);
    }
};

int main(int argc, const char * argv[]) {

    Rectangle Rect;
    
    Rect.setWidth(5);
    Rect.setHeight(7);
    
    // 输出对象的面积
    cout << "Total area: " << Rect.getArea() << endl;
    
    
    
    Rectangle2 Rect2;
    
    Rect2.setWidth(5);
    Rect2.setHeight(7);
    
    int area = Rect.getArea();
    
    // 输出对象的面积
    cout << "Total area: " << area << endl;
    
    // 输出总花费
    cout << "Total paint cost: $" << Rect2.getCost(area) << endl;
    
    return 0;
}
