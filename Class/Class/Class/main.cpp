//
//  main.cpp
//  CustomClass
//
//  Created by CavanSu on 2017/10/11.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#include <iostream>
#include "Box.hpp"

// 验证类的创建跟销毁的顺序
class Man
{
public:
    Man(void)
    {
        cout<<"我是man的构造函数，我被调用了"<<endl;
    }
    
    ~Man(void)
    {
        cout<<"我是man的析构函数，我被调用"<< endl;
    }
};

class Boy
{
public:
    Boy(void)
    {
        cout<<"我是boy的构造函数，我被调用了"<<endl;
    }
    
    ~Boy(void)
    {
        cout<<"我是boy的析构函数，我被调用了"<<endl;
    }
};

class Girl
{
public:
    Girl(void)
    {
        cout<<"我是girl的构造函数，我被调用了"<<endl;
    }
    
    ~Girl(void)
    {
        cout<<"我是girl的析构函数，我被调用了"<<endl;
    }
};

class Teenager: public Man
{
    Boy boy;
    Girl girl;
    
public:
    Teenager(void)
    {
        cout<<"我是teenager的构造函数，我被调用了"<<endl;
    }
    
    ~Teenager(void)
    {
        cout<<"我是teenager的析构函数，我被调用了"<<endl;
    }
};

int main(int argc, const char * argv[]) {
    Box box1;
    box1.setLength(6.0);
    box1.setHeight(5.0);
    box1.setBreadth(13.0);
    
    double volume = box1.getVolume();
    cout << "Box1 的体积：" << volume <<endl;
    
    Teenager teenager;
    
    return 0;
}
