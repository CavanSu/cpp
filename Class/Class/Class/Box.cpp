//
//  Box.cpp
//  Class
//
//  Created by CavanSu on 2019/3/16.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include "Box.hpp"

// 范围解析运算符 ::
void Box::setLength( double len )
{
    length = len;
}

void Box::setBreadth( double bre )
{
    breadth = bre;
}

void Box::setHeight( double hei )
{
    height = hei;
}
