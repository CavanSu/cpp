//
//  Box.hpp
//  Class
//
//  Created by CavanSu on 2019/3/16.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#ifndef Box_hpp
#define Box_hpp

#include <stdio.h>
#include <iostream>
#include <iomanip>

using namespace std;

class Box
{
    // 类成员可以被定义为 public、private 或 protected。默认情况下是定义为 private。
public:
    double length;   // 盒子的长度
    double breadth;  // 盒子的宽度
    double height;   // 盒子的高度
    
    // 在内部声明并定义
    double getVolume(void)
    {
        return length * breadth * height;
    }
    
    // 在内部声明在外部定义
    void setLength( double len );
    void setBreadth( double bre );
    void setHeight( double hei );
};

#endif /* Box_hpp */
