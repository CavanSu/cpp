//
//  main.cpp
//  AbstractedData
//
//  Created by CavanSu on 2019/2/15.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

using namespace std;

// 数据抽象是一种依赖于接口和实现分离的编程（设计）技术。
class Adder {
public:
    // 构造函数
    Adder(int i = 0)
    {
        total = i;
    }
    // 对外的接口
    void addNum(int number)
    {
        total += number;
    }
    // 对外的接口
    int getTotal()
    {
        return total;
    };
private:
    // 对外隐藏的数据
    int total;
};

int main(int argc, const char * argv[]) {
    Adder a;
    
    a.addNum(10);
    a.addNum(20);
    a.addNum(30);
    
    cout << "Total " << a.getTotal() <<endl;
    
    return 0;
}
