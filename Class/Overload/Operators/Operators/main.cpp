//
//  main.cpp
//  Operators
//
//  Created by CavanSu on 2019/1/11.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

using namespace std;

#pragma mark - 运算符重载
class Water
{
public:
    double weight = 0;
    
    Water()
    {
    }
    
    Water(const Water &obj)
    {
        cout << "调用拷贝构造函数" << endl;
    }
    
    void operator=(const Water& w)
    {
        cout << "Overload =, w.weight: " << w.weight << endl;
        weight = w.weight;
    }
    
    Water operator+(const Water& w)
    {
        Water newW;
        newW.weight = weight + w.weight;
        return newW;
    }
};

int main(int argc, const char * argv[]) {
    
    void overloadOperators();
    overloadOperators();
    
    return 0;
}

void overloadOperators()
{
    Water w1;
    w1.weight = 10;
    
    Water w2;
    w2.weight = 20;
    
    Water w3 = w1 + w2;
    cout << "w3: " << w3.weight << endl;
}
