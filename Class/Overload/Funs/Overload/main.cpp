//
//  main.cpp
//  Overload
//
//  Created by CavanSu on 2019/1/10.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

using namespace std;

#pragma mark - 函数重载
// 在同一个作用域内，可以声明几个功能类似的同名函数，但是这些同名函数的形式参数（指参数的个数、类型或者顺序）必须不同。
class PrintData
{
public:
    void print(int i) {
        cout << "整数为: " << i << endl;
    }
    
    void print(double f) {
        cout << "浮点数为: " << f << endl;
    }
    
    void print(char c[]) {
        cout << "字符串为: " << c << endl;
    }
};

#pragma mark - 运算符重载
class Water
{
public:
    double weight = 0;
    
    Water()
    {
        
    }
    
    Water(const Water& obj)
    {
        cout << "调用拷贝构造函数" << endl;
        cout << "this:" << this << endl;
        cout << "obj:" << &obj << endl;
        cout << endl;
    }
    
    void operator=(const Water& w)
    {
        cout << "Overload =, w.weight: " << w.weight << endl;
        cout << endl;
        weight = w.weight;
    }
};

int main(int argc, const char * argv[]) {
//    void overloadFuncs();
//    overloadFuncs();
    
    void diffCopyConstructorWithOverloadOperator();
    diffCopyConstructorWithOverloadOperator();
    
    return 0;
}

void overloadFuncs()
{
    PrintData pd;
    
    // 输出整数
    pd.print(5);
    // 输出浮点数
    pd.print(500.263);
    // 输出字符串
    char c[] = "Hello C++";
    pd.print(c);
}

void diffCopyConstructorWithOverloadOperator()
{
    Water w1;
    w1.weight = 10;
    
    cout << "size: " << sizeof(Water) << endl;
    
    // 这样 Water w2 = w1; 只会触发 copy constructor
    // 因为 w2 还没有完成创建，所以 w2 无法执行它的 operator = 方法
    Water w2 = w1;
    
    cout << "w2 address: " << &w2 << endl << "w1 address: " << &w1 << endl;
    cout << "w2: " << w2.weight << ", w1: " << w1.weight << endl;
    cout << endl;
    
    // 会触发 operator = 方法, 然后 copy constructor
    w2 = w1;
    cout << "w2: " << w2.weight << endl;
}
