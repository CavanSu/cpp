//
//  main.cpp
//  Polymorphism
//
//  Created by CavanSu on 2019/1/11.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

using namespace std;

class Shape {
protected:
    int width, height;
public:
    Shape(int a = 0, int  b= 0)
    {
        width = a;
        height = b;
    }
    
    // 1. int area() 导致错误输出的原因是，调用函数 area() 被编译器设置为基类中的版本，这就是所谓的静态多态，
    // 或静态链接 - 函数调用在程序执行前就准备好了。有时候这也被称为早绑定，因为 area() 函数在程序编译期间就已经设置好了。
    
    // 2. 添加 virtual, 编译器看的是指针的内容，而不是它的类型。因此，由于 tri 和 rec 类的对象的地址存储在 *shape 中，
    // 所以会调用各自的 area() 函数。
    
    // 虚函数 是在基类中使用关键字 virtual 声明的函数。在派生类中重新定义基类中定义的虚函数时，会告诉编译器不要静态链接到该函数。
    // 我们想要的是在程序中任意点可以根据所调用的对象类型来选择调用的函数，这种操作被称为动态链接，或后期绑定。
    virtual int area()
    {
        int area = width * height;
        cout << "Parent class area:" << area << endl;
        return area;
    }
    
    // 3. 纯虚函数, 基类声明，通过在声明中使用 "= 0" 来指定的， 由子类各自去实现
    virtual string color() = 0;
};

class Rectangle: public Shape
{
public:
    Rectangle(int a = 0, int b = 0): Shape(a, b) { }
    
    int area()
    {
        int area = width * height;
        cout << "Rectangle class area: " << area << endl;
        return area;
    }
    
    string color()
    {
        return "red";
    }
};

class Triangle: public Shape
{
public:
    Triangle( int a = 0, int b = 0): Shape(a, b) { }
    
    int area ()
    {
        int area = width * height / 2;
        cout << "Triangle class area: " << area << endl;
        return area;
    }
    
    string color()
    {
        return "blue";
    }
};

int main(int argc, const char * argv[]) {
    Shape *shape;
    Rectangle rec(10, 7);
    Triangle  tri(10, 5);
    
    // 存储矩形的地址
    shape = &rec;
    // 调用矩形的求面积函数 area
    shape->area();
    
    cout << shape->color() << endl;
    
    // 存储三角形的地址
    shape = &tri;
    // 调用三角形的求面积函数 area
    shape->area();
    
    cout << shape->color() << endl;
    
    return 0;
}
