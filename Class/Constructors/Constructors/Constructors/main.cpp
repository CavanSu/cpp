//
//  main.cpp
//  Constructors
//
//  Created by CavanSu on 2019/1/11.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

using namespace std;

#pragma mark - Line
class Line
{
public:
    void setLength( double len );
    double getLength( void );
    Line();  // 这是构造函数
    
private:
    // private 不能被子类访问
    double length;
};

// 成员函数定义，包括构造函数
Line::Line(void)
{
    cout << "Object is being created" << endl;
}

void Line::setLength( double len )
{
    length = len;
}

double Line::getLength( void )
{
    return length;
}

#pragma mark - SubLine
class SubLine: public Line
{
public:
    // 可以存在多个构造函数
    SubLine()
    {
        cout << "SubLine is being created" << endl;
    }
    
    SubLine(int tag)
    {
        cout << "SubLine is being created, tag: " << tag << endl;
    }
};

#pragma mark - RedLine
class RedLine: public Line
{
public:
    RedLine(double line);
    int width;
};

// 1.子类在执行自己的构造h方法前，会执行父类的构造方法
// 2.构造方法可以带参数
// 3.初始化列表来初始化字段
RedLine::RedLine(double width): width(width)
{
    this->width = width;
    cout << "RedLine is being created" << endl;
    cout << "RedLine width " << this->width << endl;
}

int main(int argc, const char * argv[]) {
    void LineTest();
    LineTest();
    
    void SubLineTest();
    SubLineTest();
    
    void RedLineTest();
    RedLineTest();
    return 0;
}

void LineTest()
{
    Line line;
    
    // 设置长度
    line.setLength(6.0);
    cout << "Length of line : " << line.getLength() << endl;
    cout << endl;
}

void SubLineTest()
{
    SubLine sLine;
    
    // 设置长度
    sLine.setLength(8.0);
    cout << "Length of SubLine : " << sLine.getLength() << endl;
    cout << endl;
    
    SubLine s2Line = SubLine(1);
    
    // 设置长度
    s2Line.setLength(8.0);
    cout << "Length of SubLine : " << s2Line.getLength() << endl;
    cout << endl;
}

void RedLineTest()
{
    RedLine rLine(10);
    cout << "Length of RedLine : " << rLine.getLength() << endl;
}
