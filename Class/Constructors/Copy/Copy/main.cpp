//
//  main.cpp
//  Copy
//
//  Created by CavanSu on 2019/1/11.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

using namespace std;

#pragma mark - Line
class Line
{
public:
    int getLength( void );
    Line(int len);             // 简单的构造函数
    Line(const Line &obj);      // 拷贝构造函数
    ~Line();                     // 析构函数
    
private:
    int *ptr;
};

// 成员函数定义，包括构造函数
Line::Line(int len)
{
    cout << "调用构造函数" << endl;
    // 为指针分配内存
    ptr = new int;
    *ptr = len;
}

Line::Line(const Line &obj)
{
    cout << "调用拷贝构造函数并为指针 ptr 分配内存" << endl;
    ptr = new int;
    *ptr = *obj.ptr; // 拷贝值
    
    cout << "ptr value: " << *ptr << endl;
}

Line::~Line(void)
{
    cout << "释放内存" << endl;
    delete ptr;
}

int Line::getLength(void)
{
    return *ptr;
}

#pragma mark - SubLine
class SubLine
{
public:
    double length;
    
    SubLine(double length)
    {
        this->length = length;
    }
    
    ~SubLine()
    {
        cout << "length: " << length << endl;
    }
};

int main(int argc, const char * argv[]) {
    Line line(10);
    cout << endl;
    
    // 传参时，触发了 copy constructor
    void display(Line obj);
    display(line);
    
    // 这里也调用了拷贝构造函数
    Line line2 = line;
  
    
    // 所有类都自带 copy constructor
    SubLine sLine(33);
    SubLine s2Line = sLine;
    
    return 0;
}

#warning 传参时，触发了 copy constructor
void display(Line obj)
{
    cout << "pre display" << endl;
    cout << "line 大小 : " << obj.getLength() << endl;
}
