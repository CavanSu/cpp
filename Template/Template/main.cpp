//
//  main.cpp
//  Template
//
//  Created by CavanSu on 2019/3/16.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>
#include "Stack.cpp"

using namespace std;

#pragma mark - 函数模板
template <typename T>
const T Max(T const a, T const b)
{
    return a < b ? b:a;
}

void funcs()
{
    int i = 39;
    int j = 20;
    cout << "Max(i, j): " << Max(i, j) << endl;
    
    double f1 = 13.5;
    double f2 = 20.7;
    cout << "Max(f1, f2): " << Max(f1, f2) << endl;
    
    string s1 = "Hello";
    string s2 = "World";
    cout << "Max(s1, s2): " << Max(s1, s2) << endl;
}

void classes()
{
    try {
        Stack<int>         intStack;  // int 类型的栈
        Stack<string>      stringStack;    // string 类型的栈
        
        // 操作 int 类型的栈
        intStack.push(7);
        cout << intStack.top() <<endl;
        
        // 操作 string 类型的栈
        stringStack.push("hello");
        cout << stringStack.top() << std::endl;
        stringStack.pop();
        stringStack.pop();
    }
    catch (exception const& ex) {
        cerr << "Exception: " << ex.what() <<endl;
    }
}

int main(int argc, const char * argv[]) {
    funcs();
    classes();
    return 0;
}
