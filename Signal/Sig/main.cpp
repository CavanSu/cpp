//
//  main.cpp
//  Sig
//
//  Created by CavanSu on 2019/3/16.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>
#include <csignal>
#include <unistd.h>

using namespace std;

#pragma mark - signal() 函数
void signalHandler(int signum)
{
    cout << "signalHandler" << endl;
    cout << "Interrupt signal (" << signum << ") received.\n";
    
    // 清理并关闭
    // 终止程序
    exit(signum);
}

void test1()
{
    // 注册信号 SIGINT 和信号处理程序
    // 第一个参数是一个整数，代表了信号的编号；第二个参数是一个指向信号处理函数的指针。
    signal(SIGINT, signalHandler);
    
    while(1){
        cout << "Going to sleep...." << endl;
        sleep(1);
    }
}

#pragma mark - raise() 函数
void test2()
{
    int i = 0;
    // 注册信号 SIGINT 和信号处理程序
    // 捕获下面 raise 发出来的信号
    signal(SIGINT, signalHandler);
    
    while(++i){
        cout << "Going to sleep...." << endl;
        if( i == 3 ){
            // 使用函数 raise() 生成信号，该函数带有一个整数信号编号作为参数
            raise(SIGINT);
        }
        sleep(1);
    }
}

int main(int argc, const char * argv[]) {
    test1();
    test2();
    return 0;
}
