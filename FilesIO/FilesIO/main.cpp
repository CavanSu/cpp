//
//  main.cpp
//  FilesIO
//
//  Created by CavanSu on 2019/2/15.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>
#include <fstream>

using namespace std;

void test1();
void test2();
void test3()

/*
 ofstream    该数据类型表示输出文件流，用于创建文件并向文件写入信息。
 ifstream    该数据类型表示输入文件流，用于从文件读取信息。
 fstream     该数据类型通常表示文件流，且同时具有 ofstream 和 ifstream 两种功能，这意味着它可以创建文件，向文件写入信息，从文件读取信息。
 */

int main(int argc, const char * argv[]) {
   
    test2();
    
    return 0;
}

void test1()
{
    /*
     ios::app    追加模式。所有写入都追加到文件末尾。
     ios::ate    文件打开后定位到文件末尾。
     ios::in    打开文件用于读取。
     ios::out    打开文件用于写入。
     ios::trunc    如果该文件已经存在，其内容将在打开文件之前被截断，即把文件长度设为 0。
     */
    
    ofstream outfile;
    outfile.open("/Users/cavansu/Desktop/file.dat", ios::out | ios::trunc );
    
    // 打开一个文件用于读写
    ifstream  afile;
    afile.open("file.dat", ios::out | ios::in );
    
    // 关闭文件
    outfile.close();
    afile.close();
}

// 读取 & 写入实例
void test2()
{
    char data[100];
    
    // 以写模式打开文件
    ofstream outfile;
    outfile.open("/Users/cavansu/Desktop/afile.dat");
    
    cout << "Writing to the file" << endl;
    cout << "Enter your name: ";
    cin.getline(data, 100);
    
    // 向文件写入用户输入的数据
    outfile << data << endl;
    
    cout << "Enter your age: ";
    cin >> data;
    cin.ignore();
    
    // 再次向文件写入用户输入的数据
    outfile << data << endl;
    
    // 关闭打开的文件
    outfile.close();
    
    // 以读模式打开文件
    ifstream infile;
    infile.open("/Users/cavansu/Desktop/afile.dat");
    
    cout << "Reading from the file" << endl;
    infile >> data;
    
    // 在屏幕上写入数据
    cout << data << endl;
    
    // 再次从文件读取数据，并显示它
    infile >> data;
    cout << data << endl;
    
    // 关闭打开的文件
    infile.close();
}

// 文件位置指针

// istream 和 ostream 都提供了用于重新定位文件位置指针的成员函数。这些成员函数包括关于 istream 的 seekg（"seek get"）和关于 ostream 的 seekp（"seek put"）。

// 查找方向可以是 ios::beg（默认的，从流的开头开始定位），
// 也可以是 ios::cur（从流的当前位置开始定位），
// 也可以是 ios::end（从流的末尾开始定位）。

void test3()
{
    // 打开一个文件用于读写
    ifstream  afile;
    afile.open("/Users/cavansu/Desktop/file.dat", ios::out | ios::in );
    
    int n = 0;
    
    // 定位到 afile 的第 n 个字节（假设是 ios::beg）
    afile.seekg(n);
    
    // 把文件的读指针从 afile 当前位置向后移 n 个字节
    afile.seekg(n, ios::cur);
    
    // 把文件的读指针从 afile 末尾往回移 n 个字节
    afile.seekg(n, ios::end);
    
    // 定位到 afile 的末尾
    afile.seekg(0, ios::end);
}
