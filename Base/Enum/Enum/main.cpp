//
//  main.cpp
//  Enum
//
//  Created by CavanSu on 2019/3/15.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

using namespace std;

#pragma mark - 枚举量的声明和定义
// 值默认分别为 int 0-6
enum enumType {Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday};

void test1()
{
    enumType weekday;
    weekday = enumType(1);
    weekday = Sunday;
    
    cout << "value: " << 1 + weekday << endl;
}

#pragma mark - 自定义枚举量的值
// 显式的设置枚举量的值：
enum enumType2 {dMonday=1, dTuesday=2, dWednesday=3, dThursday=4, dFriday=5, dSaturday=6, dSunday=7};

// 也可以只显式的定义一部分枚举量的值
// 这样 Monday、Wednesday 均被定义为 1，则 Tuesday=2，Thursday、Friday、Saturday、Sunday 的值默认分别为 2、3、4、5。
// 总结：未被初始化的枚举值的值默认将比其前面的枚举值大1。
enum enumType3 {cMonday=1, cTuesday, cWednesday=1, cThursday, cFriday, cSaturday, cSunday};

#pragma mark - 强类型枚举
// 旧枚举若是同一作用域下有两个不同的枚举类型，但含有相同的枚举常量也是不可的。例如，为何上面是 cMonday
// 且旧枚举被隐式转换为整形，用户无法自定义类型
// C++11中的强类型枚举解决这些问题

enum class Color: char {RED='R',BLUE='B',YELLOR='Y'};
enum class TrafficLight: int {RED=0, YELLOR=1, GREEN=2};

enum class Enum: unsigned int {VAL1, VAL2};

void test2()
{
    Color color = Color::RED;
    switch (color) {
        case Color::RED:
            cout << "RED" << endl;
            break;
        case Color::BLUE:
            cout << "BLUE" << endl;
            break;
        case Color::YELLOR:
            cout << "YELLOR" << endl;
            break;
        default:
            cout << "Others" << endl;
            break;
    }
    
    cout << "Color: " << (char)color << endl;
    
    cout << "VAL1: " << (int)Enum::VAL1 << endl;
}

int main(int argc, const char * argv[]) {
    test1();
    test2();
    return 0;
}


