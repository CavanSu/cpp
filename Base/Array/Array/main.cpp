//
//  main.cpp
//  Array
//
//  Created by CavanSu on 2019/1/10.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

using namespace std;

// 所有的数组都是由连续的内存位置组成。最低的地址对应第一个元素，最高的地址对应最后一个元素。

void readArray();
void pointerReadArray();
void read();

int main(int argc, const char * argv[]) {
    readArray();
    pointerReadArray();
    read();

    return 0;
}

void readArray()
{
    double balance[3];
    
    int itemSize = sizeof(double);
    int arraySize = sizeof(balance);
    int itemCount = arraySize / itemSize;
    
    cout << "itemSize = " << itemSize << endl;
    cout << "arraySize = " << arraySize << endl;
    cout << "itemCount = " << itemCount << endl;
    
    for (int i = 0; i < itemCount; i++)
    {
        balance[i] = i * 10;
        cout << "item = " << balance[i] << endl;
    }
    
    cout << "-----------------" << endl;
}

void pointerReadArray()
{
    int list[] = {0, 1, 2, 3, 4};
    cout << "list 2 = " << list[2] << endl;
    
    int *pointer = &list[0];
    int add = 1;
    int size = sizeof(int);
    
    cout << "list 0 = " << *pointer << endl;
    cout << "size = " << size << endl;
    cout << "address: " << pointer << endl;
    
    // 对指针进行加1操作，得到的将是下一个元素的地址，
    // 一个类型为T的指针移动，是以sizeof(T)为移动单位
    cout << "address + 1: " << (pointer + add) << endl;
    
    for (int i = 0; i < 5; i++)
    {
        cout << "item point = " << *(pointer + i) << endl;
    }
    
    cout << "-----------------" << endl;
}

void read()
{
    int a[6]= {1, 2, 3, 4, 5, 6};
    int b[5];
    
    // a 等于 &a 和 &a[0]
    int *pointer1 = &a[0];
    int *pointer2 = (int *)(&a + 1);             //&a+1的单位是int(*)[5]
    int next = 1;
    
    cout << "a: " << a << endl;
    cout << "&a: " << (&a[0]) << endl;
    
    // (&a + 1) 是下一个对象的地址，即a[5]
    cout << "&a+1: " << (&a + 1) << endl;
    cout << "&a+2: " << (&a + 2) << endl;
    
    // 对指针进行加1操作，得到的将是下一个元素的地址，
    // 一个类型为T的指针移动，是以sizeof(T)为移动单位
    cout << "address pointer1 + 1: " << (pointer1 + next) << endl;
    
    printf("pointer1 + 1: %d\n", *(pointer1 + next));
    printf("pointer1 = %d\n", *(pointer1));
    printf("pointer2 = %d\n", *(pointer2 - 1));
    printf("sizeof(b) = %lu\n", sizeof(b));
    
    // &b 是一个指向int型数据的指针，在64位系统中，指针的大小是8，所以sizeof(&b) = 8
    printf("sizeof(&b) = %lu\n", sizeof(&b));
}
