//
//  main.cpp
//  Funtions
//
//  Created by CavanSu on 2019/1/10.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

using namespace std;

// 函数声明
int max(int num1, int num2);
// 参数带默认值的函数，在声明时代带默认值，定义时去掉
int sum(int a, int b=20);

int main(int argc, const char * argv[]) {
   
    // 局部变量声明
    int a = 100;
    int b = 200;
    int ret;
    
    // 调用函数来获取最大值
    ret = max(a, b);
    cout << "Max value is : " << ret << endl;
    
    ret = sum(5);
    cout << "Sum value is : " << ret << endl;
    
    ret = sum(5, 10);
    cout << "Sum value is : " << ret << endl;
    
    return 0;
}

// 函数返回两个数中较大的那个数
int max(int num1, int num2)
{
    // 局部变量声明
    int result;
    
    if (num1 > num2)
        result = num1;
    else
        result = num2;
    
    return result;
}

// 参数的默认值
int sum(int a, int b)
{
    int result;
    
    result = a + b;
    
    return result;
}
