//
//  main.cpp
//  Variable
//
//  Created by CavanSu on 2019/1/10.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

using namespace std;

// extern是一个关键字，它告诉编译器存在着一个变量或者一个函数，
// 如果在当前编译语句的前面中没有找到相应的变量或者函数，
// 也会在当前文件的后面或者其它文件中定义

// 变量声明
// 全局变量
extern int a, b;
extern int c;
extern float f;

// 函数声明
int func();

float f = 100;

int main(int argc, const char * argv[]) {
  
    // 变量定义
    int a, b;
    int c;
    
    
    // 实际初始化
    a = 10;
    b = 20;
    c = a + b;
    
    cout << "c: " << c << endl ;
    
    cout << "f: " << f << endl ;
    
    // 函数调用
    int i = func();
    cout << "i = " << i << endl;
    
    return 0;
}

// 函数定义
int func()
{
    int a = 0;
    printf("a: %d\n", a);
    return a;
}
