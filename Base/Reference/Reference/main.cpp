//
//  main.cpp
//  Reference
//
//  Created by CavanSu on 2019/1/11.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

using namespace std;

// 引用变量 int& 是一个别名，也就是说，它是某个已存在变量的另一个名字
// 引用变量常替代指针使用

void test1()
{
    int i = 17;
    int& r = i;
    
    cout << "r = " << r << endl;
}

// 函数定义
void swap(int& x, int& y)
{
    int temp;
    temp = x; /* 保存地址 x 的值 */
    x = y;    /* 把 y 赋值给 x */
    y = temp; /* 把 x 赋值给 y  */
    
    return;
}

void test2()
{
    // 局部变量声明
    int a = 100;
    int b = 200;
    
    cout << "交换前，a 的值：" << a << endl;
    cout << "交换前，b 的值：" << b << endl;
    
    /* 调用函数来交换值 */
    swap(a, b);
    
    cout << "交换后，a 的值：" << a << endl;
    cout << "交换后，b 的值：" << b << endl;
}

int main(int argc, const char * argv[]) {
  
    test1();
    test2();
    return 0;
}
