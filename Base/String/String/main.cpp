//
//  main.cpp
//  String
//
//  Created by CavanSu on 2019/1/10.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

using namespace std;

void cStyleString();
void cppStyleString();
void convertedStringToCharStar();
void convertedCharStarToString();
void convertedNumberToString();
void convertedStringToNumber();

int main(int argc, const char * argv[]) {
    cStyleString();
    cppStyleString();
    convertedStringToCharStar();
    convertedCharStarToString();
    convertedNumberToString();
    convertedStringToNumber();
    return 0;
}

#pragma mark - C 风格字符串

// 字符串实际上是使用 null 字符 '\0' 终止的一维字符数组。因此，一个以 null 结尾的字符串，包含了组成字符串的字符。

void cStyleString()
{
    char greeting[6] = {'H', 'e', 'l', 'l', 'o', '\0'};
    char* pointer = greeting;
    
    cout << "greeting: " << greeting << endl;
    cout << "pointer: " << pointer << endl;
    
    // 其实不需要把 null 字符放在字符串常量的末尾。C++ 编译器会在初始化数组时，自动把 '\0' 放在字符串的末尾。
    char hello[] = "Hello";
    cout << "hello: " << hello << endl;
    
    int itemSize = sizeof(char);
    int arraySize = sizeof(hello);
    int itemCount = arraySize / itemSize;
    
    // 证明 最后插入了 '\0' 字符
    cout << "itemCount: " << itemCount << endl;
    cout << endl;
}

// 1    strcpy(s1, s2);
// 复制字符串 s2 到字符串 s1。

// 2    strcat(s1, s2);
// 连接字符串 s2 到字符串 s1 的末尾。

// 3    strlen(s1);
// 返回字符串 s1 的长度。

// 4    strcmp(s1, s2);
// 如果 s1 和 s2 是相同的，则返回 0；如果 s1<s2 则返回值小于 0；如果 s1>s2 则返回值大于 0。

// 5    strchr(s1, ch);
// 返回一个指针，指向字符串 s1 中字符 ch 的第一次出现的位置。

// 6    strstr(s1, s2);
// 返回一个指针，指向字符串 s1 中字符串 s2 的第一次出现的位置。

#pragma mark - C++ 风格字符串
void cppStyleString()
{
    string str1 = "Hello";
    string str2 = "World";
    string str3;
    int len;
    
    // 复制 str1 到 str3
    str3 = str1;
    cout << "str3 : " << str3 << endl;
    
    // 连接 str1 和 str2
    str3 = str1 + str2;
    cout << "str1 + str2 : " << str3 << endl;
    
    // 连接后，str3 的总长度
    len = (int)str3.size();
    cout << "str3.size() :  " << len << endl;
}

#pragma mark - string 字符串转 char*
void convertedStringToCharStar()
{
    // 1.
    string str1 = "Hello";
    const char* pointer = str1.data();
    cout << "pointer: " << pointer << endl;
    
    // 2.
    const char* pointer2 = str1.c_str();
    cout << "pointer2: " << pointer2 << endl;
    
    // 3.
    char sArray[6];
    str1.copy(sArray, 5, 0);
    char* temp = &sArray[0];
    *(temp + 5) = '\0';
    cout << "sArray: " << sArray << endl;
}

#pragma mark - char* 转 string
void convertedCharStarToString()
{
    string str;
    char greeting[6] = {'H', 'e', 'l', 'l', 'o', '\0'};
    char* p = &greeting[0];
    str = p;
    cout << str << endl;
}

#pragma mark - 数值 转 string
void convertedNumberToString()
{
    int number = 10;
    float number2 = 20.0;
    
    string text = to_string(number) + to_string(number2);
    cout << "text: " << text << endl;
}

#pragma mark - string 转 数值
void convertedStringToNumber()
{
    string intStr = "234";
    int intNumber = atoi(intStr.data());
    cout << "intNumber: " << intNumber << endl;
    
    string doubleStr = "20.00";
    float doubleNumber = atof(doubleStr.data());
    cout << "doubleNumber: " << doubleNumber << endl;
}
