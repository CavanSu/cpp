//
//  main.cpp
//  Struct
//
//  Created by CavanSu on 2019/1/11.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

using namespace std;

// 声明一个结构体类型 Books
struct Book
{
    // 成员列表
    char  title[50];
    char  author[50];
    char  subject[100];
    string date;
    int   book_id;
} book3; // book3: 声明结构体时顺带创建一个结构体变量，不推荐

/*
 struct Books
 {
     // 成员列表
     char  title[50];
     char  author[50];
     char  subject[100];
     string date;
     int   book_id;
 } ; 声明
 **/

struct Library
{
    Book books[2];
};

void test1()
{
    // 创建结构体变量
    // struct Books book1; // struct 可以省略
    Book book1;        // 定义结构体类型 Books 的变量 Book1
    
    strcpy(book1.title, "C++ 从入门到精通");
    strcpy(book1.author, "AAA");
    strcpy(book1.subject, "Computer");
    
    book1.date = "2019.1.11";
    
    clog << "title:" << book1.title << endl;
    clog << "author: " << book1.author << endl;
    clog << "subject: " << book1.subject << endl;
    clog << "date: " << book1.date << endl;
    
    // 创建结构体变量， 直接赋值
    Book book2 = {"J++ 从入门到精通", "bbb", "ccc", "ddd"};
    clog << "title: " << book2.title << endl;
    clog << "author: " << book2.author << endl;
    clog << "subject: " << book2.subject << endl;
    clog << "date: " << book2.date << endl;
}

void testArray()
{
    Book books[3] = {{"aaa", "bbb", "ccc", "dddd"},
                     {"aba", "bbb", "ccc", "dddd"},
                     {"aac", "bbb", "ccc", "dddd"}
    };
    
    for (int i = 0; i < 3; i ++)
    {
        Book book = books[i];
        clog << "title: " << book.title << endl;
        clog << "author: " << book.author << endl;
        clog << "subject: " << book.subject << endl;
        clog << "date: " << book.date << endl;
        clog << "-----------" << endl;
    }
}

void testPointer()
{
    Book book2 = {"J++ 从入门到精通", "bbb", "ccc", "ddd"};
    Book* pointer = nullptr;
    
    pointer = &book2;
    
    strcpy(pointer->title, "java");
    (*pointer).date = "123";
    
    clog << "title: " << book2.title << endl;
    clog << "author: " << book2.author << endl;
    clog << "subject: " << book2.subject << endl;
    clog << "date: " << book2.date << endl;
}

void structInsertStruct()
{
    Book books[2] = {{"aaa", "bbb", "ccc", "dddd"},
                     {"aba", "bbb", "ccc", "dddd"}
    };
    
    Library lib = {{{"123", "456", "789", "dddd"},
                   {"789", "456", "123", "dddd"}}};
    
    for (int i = 0; i < 2; i ++)
    {
        Book book =  lib.books[i];
        clog << "title: " << book.title << endl;
        clog << "author: " << book.author << endl;
        clog << "subject: " << book.subject << endl;
        clog << "date: " << book.date << endl;
        clog << "-----------" << endl;
    }
    
    size_t size = sizeof(books);
    
    memcpy(lib.books, books, size);
    
    for (int i = 0; i < 2; i ++)
    {
        Book book =  lib.books[i];
        clog << "title: " << book.title << endl;
        clog << "author: " << book.author << endl;
        clog << "subject: " << book.subject << endl;
        clog << "date: " << book.date << endl;
        clog << "-----------" << endl;
    }
}

void testParameters(Book book1, Book* book2)
{
    book1.date = "456789";
    book2->date = "iiiiiii";
}

void test2()
{
    Book book1;        // 定义结构体类型 Books 的变量 Book1
    
    strcpy(book1.title, "C++ 从入门到精通");
    strcpy(book1.author, "AAA");
    strcpy(book1.subject, "Computer");
    
    book1.date = "2019.1.11";
    
    // 创建结构体变量， 直接赋值
    Book book2 = {"J++ 从入门到精通", "bbb", "ccc", "ddd"};
    
    testParameters(book1, &book2);
    
    clog << "title:" << book1.title << endl;
    clog << "author: " << book1.author << endl;
    clog << "subject: " << book1.subject << endl;
    clog << "date: " << book1.date << endl;
    
    clog << "title: " << book2.title << endl;
    clog << "author: " << book2.author << endl;
    clog << "subject: " << book2.subject << endl;
    clog << "date: " << book2.date << endl;
}

int main(int argc, const char * argv[]) {
//    test1();
//    testArray();
//    testPointer();
//    structInsertStruct();
    test2();
    
    return 0;
}
