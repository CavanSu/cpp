//
//  Support.cpp
//  Modifier
//
//  Created by CavanSu on 2019/1/10.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include "Support.hpp"
#include <iostream>

using namespace std;

int number = 18;

void write_extern(void)
{
    cout << "Count is " << number << endl;
}
