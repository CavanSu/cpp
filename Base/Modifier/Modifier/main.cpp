//
//  main.cpp
//  Modifier
//
//  Created by CavanSu on 2019/1/10.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

void func(void);
void staticTest();
void externTest();

static int count = 10; /* 全局变量 */

int main(int argc, const char * argv[]) {
   
    staticTest();
    externTest();
    
    return 0;
}

#pragma mark - static

// 修饰局部变量: 在程序的生命周期内保持局部变量的存在，而不需要在每次它进入和离开作用域时进行创建和销毁。因此，使用 static 修饰局部变量可以在函数调用之间保持局部变量的值

// 修饰全局变量: 使变量的作用域限制在声明它的文件内, 不可在文件外被调用

// 修饰类数据成员: 会导致仅有一个该成员的副本被类的所有对象共享

void func( void )
{
    static int i = 5; // 局部静态变量
    i++;
    std::cout << "变量 i 为 " << i ;
    std::cout << " , 变量 count 为 " << count << std::endl;
}

void staticTest()
{
    while(count--)
    {
        func();
    }
}

#pragma mark - extern
// 可以在其他文件中使用 extern 来得到已定义的变量或函数的引用
// extern是一个关键字，它告诉编译器存在着一个变量或者一个函数，
// 如果在当前编译语句的前面中没有找到相应的变量或者函数，
// 也会在当前文件的后面或者其它文件中定义

// 当一个全局变量不被 static 修饰时， 可以通过 extern 在别的文件指向这个全局变量
extern int number;

// 引用其他文件的函数
extern void write_extern(void);

void externTest()
{
    std::cout << "number = " << number << std::endl;
    
    write_extern();
}
