//
//  main.cpp
//  IO
//
//  Created by CavanSu on 2019/1/11.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>
#include <iomanip>

using namespace std;

void outAndInput();
void setwTest();
void errorOut();
void logOut();

int main(int argc, const char * argv[]) {
    std::cout << __cplusplus << std::endl;
    
    errorOut();
    logOut();
    setwTest();
    outAndInput();
    
    return 0;
}

void outAndInput()
{
    char name[50];
    
    cout << "请输入您的名称： ";
    cin >> name;
    cout << "您的名称是： " << name << endl;
}

void setwTest()
{
    float a = 3.14;
    int b = 5;
    char c = 'A';
    
    /*
     setw(int n)是c++中在输出操作中使用的字段宽度设置，设置输出的域宽，n表示字段宽度。只对紧接着的输出有效，紧接着的输出结束后又变回默认的域宽。
     当后面紧跟着的输出字段长度小于n的时候，在该字段前面用空格补齐；当输出字段长度大于n时，全部整体输出。
     */
    
    cout << "a = " << setw(6) << a << endl << "b = " << setw(6) << b << endl << "c = " << setw(6) << c << endl;
}

void errorOut()
{
    char str[] = "Unable to read....";
    cerr << "Error message : " << str << endl;
}

void logOut()
{
    char str[] = "read....";
    clog << "Info : " << str << endl;
}
