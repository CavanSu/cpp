//
//  main.cpp
//  HelloWorld
//
//  Created by CavanSu on 2017/10/11.
//  Copyright © 2017年 CavanSu. All rights reserved.
//

#include <iostream>

using namespace std;

int main(int argc, const char * argv[]) {
   
    cout << "Hello world \n";
    
    printf("Hello world again \n");
    
    // addtion method test
    void addtion();
    addtion();
    
    // addtion method test again
    int addtionAgain(int x, int y);
    int a = addtionAgain(3, 4);
    cout << "a = " << a << endl;
    
    return 0;
}

void addtion()
{
    int a, b, sum;
    
    cout << "Inputing ....... \n";
    
    cin >> a >> b;
    
    sum = a + b;
    
    cout << "a+b = " << sum << endl;
}

int addtionAgain(int x, int y)
{
    cout << "use this func \n";
    return x + y;
}
