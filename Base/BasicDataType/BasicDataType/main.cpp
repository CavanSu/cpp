//
//  main.cpp
//  BasicDataType
//
//  Created by CavanSu on 2019/1/10.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>
using namespace std;

void coutBasicDataType(); // 函数声明，c++ 需要在使用前先声明函数

int main(int argc, const char * argv[]) {
    coutBasicDataType();
    
    void typedefTest();
    typedefTest();
    
    void enumTest();
    enumTest();
    
    return 0;
}

#pragma mark - BasicDataType
void coutBasicDataType()
{
    cout << "bool: \t\t" << "所占字节数：" << sizeof(bool);
    cout << "\t最大值：" << (numeric_limits<bool>::max)();
    cout << "\t\t最小值：" << (numeric_limits<bool>::min)() << endl;
    
    cout << "char: \t\t" << "所占字节数：" << sizeof(char);
    cout << "\t最大值：" << (numeric_limits<char>::max)();
    cout << "\t\t最小值：" << (numeric_limits<char>::min)() << endl;
    
    cout << "signed char: \t" << "所占字节数：" << sizeof(signed char);
    cout << "\t最大值：" << (numeric_limits<signed char>::max)();
    cout << "\t\t最小值：" << (numeric_limits<signed char>::min)() << endl;
    
    cout << "float: \t\t" << "所占字节数：" << sizeof(float);
    cout << "\t最大值：" << (numeric_limits<float>::max)();
    cout << "\t最小值：" << (numeric_limits<float>::min)() << endl;
    
    cout << endl;
}

#pragma mark - typedef
// typedef: 可以使用 typedef 为一个已有的类型取一个新的名字
typedef int feet;

void typedefTest()
{
    cout << "int: \t\t" << "所占字节数：" << sizeof(int);
    cout << "\t最大值：" << (numeric_limits<int>::max)();
    cout << "\t最小值：" << (numeric_limits<int>::min)() << endl;
    
    cout << "feet: \t\t" << "所占字节数：" << sizeof(feet);
    cout << "\t最大值：" << (numeric_limits<feet>::max)();
    cout << "\t最小值：" << (numeric_limits<feet>::min)() << endl;
    
    cout << endl;
}

#pragma mark - enum
// 默认情况下，第一个名称的值为 0，第二个名称的值为 1，第三个名称的值为 2，以此类推
enum color
{
    red, blue, green
};

// 也可以给名称赋予一个特殊的值，只需要添加一个初始值即可
// 之后的枚举值也是往后加 1
enum orientation
{
    up = 5, down, left, right
};

void enumTest()
{
    color wall = red;
    color desk = blue;
    
    cout << wall << endl;
    cout << desk << endl;
    
    orientation upO = up;
    orientation down0 = down;
    
    cout << "up: " << upO << endl;
    cout << "down: " << down0 << endl;
}
