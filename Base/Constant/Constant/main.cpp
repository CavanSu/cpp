//
//  main.cpp
//  Constant
//
//  Created by CavanSu on 2019/1/10.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>
using namespace std;

#pragma mark - define

#define LENGTH 10
#define WIDTH  5
#define NEWLINE '\n'

int main(int argc, const char * argv[]) {
    void defineTest();
    defineTest();
    
    void constTest();
    constTest();
    
    void threadLocalTest();
    threadLocalTest();
    
    return 0;
}

void defineTest()
{
    int area;
    
    area = LENGTH * WIDTH;
    cout << area;
    cout << NEWLINE;
}

#pragma mark - const
// const 类型的对象在程序执行期间不能被修改改变。
void constTest()
{
    const int  cLENGTH = 10;
    const int  cWIDTH  = 5;
    const char cNEWLINE = '\n';
    int area;
    
    area = cLENGTH * cWIDTH;
    cout << area;
    cout << cNEWLINE;
}

#pragma mark - thread_local
// 使用 thread_local 修饰声明的变量仅可在它在其上创建的线程上访问。 变量在创建线程时创建，并在销毁线程时销毁。 每个线程都有其自己的变量副本。

void threadLocalTest()
{
    thread_local string s = "main";
    cout << "thread = " << s << endl;
}
