//
//  main.cpp
//  Pointer
//
//  Created by CavanSu on 2019/1/10.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

using namespace std;

// 32位 指针长度 4 字节，
// 64位 指针长度 8 字节

void test();
void test1(int* a, int* b);

int main(int argc, const char * argv[]) {
  
    int a = 10;
    int b = 20;
    test1(&a, &b);
    
    cout << "a: " << a << endl;
    cout << "b: " << b << endl;
    
    return 0;
}

void test()
{
    int  var1;
    char var2[10];
    
    // 空指针，如果访问会报错
    // 0 ~ 255 的地址区间为系统持有，访问会报错
    // NULL 是地址为 0
    int* iPointer = NULL;
    char* cPointer = NULL;
    
    iPointer = &var1;
    
    cout << "var1 变量的地址： ";
    cout << &var1 << endl;
    cout << "iPointer 地址: " << iPointer << endl;
    cout << "iPointer size: " << sizeof(iPointer) << endl;
    
    cout << "var2 变量的地址： ";
    cout << &var2 << endl;
    cout << &var2[0] << endl;
    cout << "cPointer size: " << sizeof(cPointer) << endl;
}

void test1(int* a, int* b)
{
    cout << "a: " << *a << endl;
    cout << "b: " << *b << endl;
    
    *a = 44;
    *b = 55;
}
