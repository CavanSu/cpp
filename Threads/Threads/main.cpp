//
//  main.cpp
//  Threads
//
//  Created by CavanSu on 2019/3/17.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>
// 必须的头文件
#include <pthread.h>
#include <unistd.h>

using namespace std;

#define NUM_THREADS 5

#pragma mark - 线程创建不带参数
// 线程的运行函数
void* say_hello(void* args)
{
    cout << "Hello Runoob！" << endl;
    pthread_exit(NULL);
}

void test1 ()
{
    // 定义线程的 id 变量，多个变量使用数组
    pthread_t tids[NUM_THREADS];
    for(int i = 0; i < NUM_THREADS; ++i)
    {
        //参数依次是：创建的线程id，线程参数，调用的函数，传入的函数参数
        int ret = pthread_create(&tids[i], NULL,
                                 say_hello, NULL);
        if (ret != 0)
        {
            cout << "pthread_create error: error_code = " << ret << endl;
        }
    }
    // 等各个线程退出后，进程才结束，否则进程强制结束了，线程可能还没反应过来；
    // pthread_exit(NULL); // 可能是主线程被终止，而子线程还没被 kill ，造成进程停不下来。不过更大应该是 xcode 的bug, 直接运行 exec 没有问题。
    
    cout << "after exit" << endl;
}

#pragma mark - 线程创建带参数
void* printHello(void* threadid)
{
    // 对传入的参数进行强制类型转换，由无类型指针变为整形数指针，然后再读取
    int tid = *((int*)threadid);
    cout << "Hello Runoob! 线程 ID = " << tid << "\n" << endl;
    pthread_exit(NULL);
}

void test2()
{
    pthread_t threads[NUM_THREADS];
    int indexes[NUM_THREADS];// 用数组来保存i的值
    int error;
    int i;
    
    for(i = 0; i < NUM_THREADS; i++) {
        cout << "main() : 创建线程, i = " << i << "\n" << endl;
        indexes[i] = i; //先保存i的值
        
        // 传入的时候必须强制转换为void* 类型，即无类型指针
        error = pthread_create(&threads[i], NULL,
                            printHello, (void* )&(indexes[i]));
        
        // 防止多个子线程同时访问 printHello(void* threadid)
        sleep(1);
        
        if (error) {
            cout << "Error:无法创建线程," << error << endl;
            exit(-1);
        }
    }
//    pthread_exit(NULL);
}

int main(int argc, const char * argv[]) {
    test1();
    test2();
    return 0;
}
