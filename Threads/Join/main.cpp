//
//  main.cpp
//  ConnectOrNot
//
//  Created by CavanSu on 2019/3/17.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>
#include <pthread.h>
#include <unistd.h>

using namespace std;

#pragma mark - pthread_join
// pthread_join
// 1. 当调用 pthread_join() 时，当前线程会处于阻塞状态，直到被调用的线程结束后，当前线程才会重新开始执行
// 2. 对 pthread_join() 里的线程进行资源回收：如果一个线程是非分离的（默认情况下创建的线程都是非分离）并且没有对该线程使用 pthread_join() 的话，
// 该线程结束后并不会释放其内存空间，这会导致该线程变成了“僵尸线程”。
// 但是释放的仅仅是系统空间，必须手动清除程序分配的空间，比如 malloc() 分配的空间。
// 被连接的线程必须是非分离的，否则连接会出错

void *wait(void *t)
{
    int tid;
    tid = *(int*)t;
    
    cout << "SubThread Sleeping in thread " << endl;
    cout << "SubThread with id : " << tid << "  ...exiting " << endl;
    
    sleep(3);
    cout << endl;
    pthread_exit(NULL);
}

int main(int argc, const char * argv[]) {
    
    int error;
    int i = 1;
    pthread_t subThread;
    pthread_attr_t attr;
    void *status;
    
    // 初始化并设置线程为可连接的（joinable）
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    error = pthread_create(&subThread, &attr, wait, (void *)&i);
    if (error){
        cout << "Error:unable to create thread," << error << endl;
        exit(-1);
    }
    
    // 删除属性
    pthread_attr_destroy(&attr);
    
    // 等待子线程结束
    error = pthread_join(subThread, &status);
    if (error){
        cout << "Error: unable to join," << error << endl;
        exit(-1);
    }
    
    cout << "Main: completed thread id :" << i ;
    cout << "  exiting with status :" << status << endl;
    
    cout << "Main: program exiting." << endl;
    
    return 0;
}
