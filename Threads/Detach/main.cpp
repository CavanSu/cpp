//
//  main.cpp
//  Detach
//
//  Created by CavanSu on 2019/3/17.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>
#include <pthread.h>
#include <unistd.h>

using namespace std;

#pragma mark - pthread_join
// pthread_detach
// pthread_detach() 即主线程与子线程分离，两者相互不干涉，子线程结束同时子线程的资源自动回收,
// 但是释放的仅仅是系统空间，必须手动清除程序分配的空间，比如 malloc() 分配的空间。

void *wait(void *t)
{
    int tid;
    tid = *(int*)t;
    
    cout << "SubThread Sleeping in thread " << endl;
    cout << "SubThread with id : " << tid << "  ...exiting " << endl;
    
    sleep(3);
    cout << endl;
    
    cout << "SubThread After sleep" << endl;
    pthread_exit(NULL);
}

int main(int argc, const char * argv[]) {
    int error;
    int i = 1;
    pthread_t subThread;
    pthread_attr_t attr;
    void *status;
    
    // 初始化并设置线程为可连接的（joinable）
    // 本 case 中， PTHREAD_CREATE_JOINABLE 与 PTHREAD_CREATE_DETACHED 一样
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    
    error = pthread_create(&subThread, &attr, wait, (void *)&i);
    if (error){
        cout << "Error:unable to create thread," << error << endl;
        exit(-1);
    }
    
    // 删除属性
    pthread_attr_destroy(&attr);
    
    // 不会等待子线程结束
    error = pthread_detach(subThread);
    if (error){
        cout << "Error: unable to join," << error << endl;
        exit(-1);
    }
    
    cout << "Main: completed thread id :" << i ;
    cout << "  exiting with status :" << status << endl;
    
    cout << "Main: program exiting." << endl;
    return 0;
}
