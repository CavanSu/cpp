//
//  main.cpp
//  MultiParameters
//
//  Created by CavanSu on 2019/3/17.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <unistd.h>

using namespace std;

#define NUM_THREADS 5

struct thread_data{
    int  thread_id;
    char* message;
};

void* printHello(void* threadarg)
{
    struct thread_data* my_data;
    
    my_data = (struct thread_data*) threadarg;
    
    cout << "Thread ID : " << my_data->thread_id ;
    cout << " Message : " << my_data->message << endl;
    cout << endl;
    pthread_exit(NULL);
}

int main(int argc, const char * argv[]) {
    
    pthread_t threads[NUM_THREADS];
    struct thread_data td[NUM_THREADS];
    int error;
    int i;
    
    for(i = 0; i < NUM_THREADS; i++) {
        cout << "main() : creating thread, " << i << endl;
        
        string message = "This is message: " + to_string(i);
        
        td[i].thread_id = i;
        td[i].message = (char*)message.c_str();
        error = pthread_create(&threads[i], NULL,
                            printHello, (void*)&td[i]);
        
        // 防止多个子线程同时访问 printHello(void* threadid)
        sleep(1);
        
        if (error){
            cout << "Error:unable to create thread," << error << endl;
            exit(-1);
        }
    }
    
    return 0;
}
