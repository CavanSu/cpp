//
//  main.cpp
//  Exception
//
//  Created by CavanSu on 2019/3/15.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

using namespace std;

#pragma mark - 抛出异常
double division(int a, int b)
{
    if(b == 0)
    {
        throw "Division by zero condition!";
    }
    return (a/b);
}

enum class AError: int {None=0, DivisionZero, Unknown};

double division2(int a, int b)
{
    if(b == 0)
    {
        AError error = AError::DivisionZero;
        throw error;
    }
    return (a/b);
}

void test1()
{
    int x = 50;
    int y = 0;
    double z = 0;
    
    try {
        z = division(x, y);
        cout << z << endl;
    } catch (const char* msg) {
        cerr << msg << endl;
    }
    
    try {
        z = division2(x, y);
        cout << z << endl;
    } catch (AError msg) {
        cerr << "error " << (int)msg << endl;
    }
    
    try {
        z = division2(x, y);
        cout << z << endl;
    } catch (...) { // 能处理任何异常的代码
        cerr << "error" << endl;
    }
}

#pragma mark - 定义新的异常
#include <exception>

class MyException : public exception
{
public:
    const char * what () const throw ()
    {
        return "C++ Exception";
    }
};

void test2()
{
    try {
        throw MyException();
    } catch(MyException& e) {
        cout << "MyException caught" << std::endl;
        cout << e.what() << std::endl;
    } catch(std::exception& e) {
        //其他的错误
        cout << "unknown error" << endl;
    }
}
    
int main(int argc, const char * argv[]) {
    test1();
    test2();
    return 0;
}
