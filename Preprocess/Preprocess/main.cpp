//
//  main.cpp
//  Preprocess
//
//  Created by CavanSu on 2019/3/16.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

using namespace std;

// #define 预处理
#define PI 3.14159

// 参数宏
#define MIN(a,b) (a<b ? a : b)
void test()
{
    int i, j;
    i = 100;
    j = 30;
    cout <<"较小的值为：" << MIN(i, j) << endl;
}

// 条件编译
void test2()
{
#ifdef DEBUG
    cerr <<"Variable x = " << 3 << endl;
#endif
}

// C++ 中的预定义宏
void test3()
{
    cout << "Value of __LINE__ : " << __LINE__ << endl;
    cout << "Value of __FILE__ : " << __FILE__ << endl;
    cout << "Value of __DATE__ : " << __DATE__ << endl;
    cout << "Value of __TIME__ : " << __TIME__ << endl;
}

int main(int argc, const char * argv[]) {
    test();
    test3();
    return 0;
}
