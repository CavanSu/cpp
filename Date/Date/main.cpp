//
//  main.cpp
//  Date
//
//  Created by CavanSu on 2019/1/11.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>
#include <ctime>

using namespace std;

void structTmTest();
void structTmTest2();

int main(int argc, const char * argv[]) {
    structTmTest();
    structTmTest2();
    
    return 0;
}

#pragma mark - struct tm

#define BST (+1)
#define CCT (+8)

void structTmTest()
{
    time_t rawtime;
    time(&rawtime);
    
    struct tm *info;
    
    /* 获取 GMT 时间 */
    info = gmtime(&rawtime);
    
    printf("当前的世界时钟：\n");
    printf("伦敦：%2d:%02d\n", (info->tm_hour+BST)%24, info->tm_min);
    printf("中国：%2d:%02d\n", (info->tm_hour+CCT)%24, info->tm_min);
}

void structTmTest2()
{
    time_t rawtime;
    time(&rawtime);
    
    struct tm *info = localtime(&rawtime);
    
    printf("Local：%02d-%02d,%2d:%02d\n", (info->tm_mon + 1), info->tm_mday, info->tm_hour, info->tm_min);
}
