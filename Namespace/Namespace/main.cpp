//
//  main.cpp
//  Namespace
//
//  Created by CavanSu on 2019/3/16.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

// using 指令也可以用来指定命名空间中的特定项目。例如，只打算使用 std 命名空间中的 cout 部分，可以使用如下的语句：
using std::cout;

// 第一个命名空间
namespace first_space {
    void func() {
        cout << "Inside first_space" << std::endl;
    }
}

// 第二个命名空间
namespace second_space {
    void func() {
        cout << "Inside second_space" << std::endl;
    }
}

// 第三个命名空间
namespace third_space {
    void func() {
        cout << "Inside third_space" << std::endl;
    }
}

// using namespace 指令，这样在使用命名空间时就可以不用在前面加上命名空间的名称。
// 这个指令会告诉编译器，后续的代码将使用指定的命名空间中的名称。
using namespace third_space;

int main(int argc, const char * argv[]) {
   
    // 调用第一个命名空间中的函数
    first_space::func();
    
    // 调用第二个命名空间中的函数
    second_space::func();
    
    // third_space
    func();
    
    return 0;
}
