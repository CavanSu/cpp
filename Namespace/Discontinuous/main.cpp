//
//  main.cpp
//  Discontinuous
//
//  Created by CavanSu on 2019/3/16.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

// 命名空间可以定义在几个不同的部分中，因此命名空间是由几个单独定义的部分组成的。一个命名空间的各个组成部分可以分散在多个文件中。
// 所以，如果命名空间中的某个组成部分需要请求定义在另一个文件中的名称，则仍然需要声明该名称。

using namespace std;

namespace Discontinuous {
    void test() {
        cout << "test" << endl;
    }
}

namespace Discontinuous {
    void test2() {
        cout << "test2" << endl;
    }
}

using namespace Discontinuous;

int main(int argc, const char * argv[]) {
    test();
    test2();
    return 0;
}
