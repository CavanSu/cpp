//
//  main.cpp
//  Nested
//
//  Created by CavanSu on 2019/3/16.
//  Copyright © 2019 CavanSu. All rights reserved.
//

#include <iostream>

using namespace std;

// 命名空间可以嵌套，您可以在一个命名空间中定义另一个命名空间，如下所示：

// 第一个命名空间
namespace first_space{
    void func(){
        cout << "Inside first_space" << endl;
    }
    // 第二个命名空间
    namespace second_space{
        void func(){
            cout << "Inside second_space" << endl;
        }
    }
}

// 访问 namespace:name1 中的成员
using namespace first_space;
// 访问 namespace_name2 中的成员
using namespace first_space::second_space;

int main(int argc, const char * argv[]) {
    // 调用第二个命名空间中的函数
    second_space::func();
    return 0;
}
